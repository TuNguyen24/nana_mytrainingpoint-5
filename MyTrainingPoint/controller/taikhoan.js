// route middleware to make sure
exports.isLoggedIn= function(req, res, next) {
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/login');
}
exports.isNotLoggedIn = function(req,res,next){
    // if user is authenticated in the session, carry on
    if (!req.isAuthenticated())
    return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}

exports.isAdminLoggedin = function(req,res,next){
    if (req.isAuthenticated()){
        req.getConnection(function(err,connection){
            connection.query("select TaiKhoan from admin where TaiKhoan='"+req.user+"'",function(err,data){
                if(data.length != 0 && req.isAuthenticated()){
                    return next();
                }
                else{
                    res.redirect('/sinhvien');
                }
            })
        })
    }
    else{
        res.redirect('/login');
    }
}

exports.isSVLoggedin = function(req,res,next){
    if (req.isAuthenticated()){
        req.getConnection(function(err,connection){
            connection.query("select MSSV from sinhvien where MSSV='"+req.user+"'",function(err,data){
                if(data.length != 0 && req.isAuthenticated()){
                    return next();
                }
                else{
                    res.redirect('/admin');
                }
            })
        })
    }
    else{
        res.redirect('/login');
    }
}